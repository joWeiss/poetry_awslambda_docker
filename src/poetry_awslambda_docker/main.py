from functools import lru_cache

import requests


def lambda_handler(event, context):
    ip = get(event.get("url", "http://ip.42.pl/raw"))
    return f"My IP is {ip}"


@lru_cache
def get(url: str) -> str:
    return requests.get(url).text
