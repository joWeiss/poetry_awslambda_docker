{
  description = "AWS Lambda function using poetry2nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, }:
    {
      # Nixpkgs overlay providing the application
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          # The development environment
          pyenv39 = prev.poetry2nix.mkPoetryEnv {
            projectDir = ./.;
            preferWheels = true;
            python = prev.python39;
            editablePackageSources = {
              myApp = ./src;
              myTests = ./tests;
            };
            groups = [ "dev" ];
            extraPackages = ps: [ ps.setuptools ps.tox ps.black ];
          };
          # The application for installation in the docker image
          app39 = prev.poetry2nix.mkPoetryApplication {
            projectDir = ./.;
            python = prev.python39;
            checkGroups = [ "dev" ];
            preferWheels = true;
          };
          # We have to patch awslambdaric because it really wants to use simplejson==3.17.5,
          # wich is *not* the default version in nixpkgs;
          awslambdaric = prev.python39Packages.awslambdaric.overrideAttrs
            (old: {
              postPatch = ''
                substituteInPlace requirements/base.txt \
                    --replace 'simplejson==3.17.5' 'simplejson~=3.18.0'
              '';
              propagatedBuildInputs = (old.propagatedBuildInputs or [ ])
                ++ [ prev.python39Packages.simplejson ];
            });
        })
      ];
    } // (flake-utils.lib.eachDefaultSystem (system:
      let
        # general
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
        py39 = pkgs.pyenv39.env.overrideAttrs
          (oldAttrs: { buildInputs = [ pkgs.poetry ]; });
        awsLambdaPython =
          pkgs.python39.withPackages (ps: [ pkgs.awslambdaric pkgs.app39 ]);
        awsLambdaBase = name: handler:
          pkgs.dockerTools.buildLayeredImage {
            name = name;
            tag = "latest";
            contents = [ awsLambdaPython ];
            config = {
              EntryPoint =
                [ "${awsLambdaPython}/bin/python" "-m" "awslambdaric" ];
              Cmd = [ "${handler}" ];
            };
          };
      in {
        packages = {
          default = self.packages.${system}.venv39;
          venv39 = pkgs.pyenv39;
          awsLambda = awsLambdaBase "poetry_awslambda_docker"
            "poetry_awslambda_docker.main.lambda_handler";
        };
        devShells = {
          default = py39;
          py39 = py39;
        };
      }));
}
