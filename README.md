# Deploy a Python app to AWS Lambda with NIX Flakes

The "regular" workflow looks more or less like this:

1. Use poetry to create the virtual environment (_venv_) for the Python project.
2. Use this _venv_ in your editor of choice for Python intellisense/code-completion/you-name-it.
3. Push the code to your remote (e.g. GitLab), so that a CI can build the Docker Images and Push
   them to AWS Elastic Container Repository.
4. Load the image for the AWS Lambda function to start using it.

The steps to build the Docker Image can be described in a single [`flake.nix`](./flake.nix).

Now we can build and load the docker image like so:

1. `nix build .#awsLambda`
2. `docker load < result`
3. (optional) `docker image inspect poetry_awslambda_docker:latest`

To actually test this, follow the official AWS documentation for
the AWS Lambda RIE from [here](https://docs.aws.amazon.com/lambda/latest/dg/images-test.html#images-test-add):

1. From your project directory, run the following command to download the RIE (x86-64 architecture) from GitHub and install it on your local machine.
    ```bash
    mkdir -p ~/.aws-lambda-rie && curl -Lo ~/.aws-lambda-rie/aws-lambda-rie \
    https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie \
    && chmod +x ~/.aws-lambda-rie/aws-lambda-rie     
    ```

    To download the RIE for arm64 architecture, use the previous command with a different GitHub download url.
    ```
     https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie-arm64 \            
    ```

2. Run your Lambda function using the docker run command.
    ```bash
    docker run -d -v ~/.aws-lambda-rie:/aws-lambda -p 9000:8080 \
      --entrypoint /aws-lambda/aws-lambda-rie \
      poetry_awslambda_docker:latest poetry_awslambda_docker.main.lambda_handler
    ```
    This runs the image as a container and starts up an endpoint locally at `localhost:9000/2015-03-31/functions/function/invocations`.

3. Post an event to the following endpoint using a curl command:
    ```bash
    curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'
    ```
   This command invokes the function running in the container image and returns a response.


This looks like this:

![The logs from the docker run command](./docs/docker_run.png)
![The HTTP request and response to the local function](./docs/calls.png)

As you can see, the first request took about 105ms, but all following requests
took less then 1ms to complete.
